﻿//#define ENABLE_POPMOVIE
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TimeCue
{
    [SerializeField]
    private float fActivationTime;
    [SerializeField]
    private float fDuration;
    [SerializeField]
    private float fDistanceFromLive;

    public TimeCue(float fActivationTime,float fDuration, float fDistanceFromLive)
    {
        this.fActivationTime = fActivationTime;
        this.fDuration = fDuration;
        this.fDistanceFromLive = fDistanceFromLive;
    }

    public float GetActivationTime()
    {
        return fActivationTime;
    }

    public float GetDuration()
    {
        return fDuration;
    }

    public float GetDistanceFromLive()
    {
        return fDistanceFromLive;
    }
}

public class FrameBuffering : MonoBehaviour {

	public class FrameCache
	{
		public RenderTexture	Frame;
		public uint				Time;
	};

    //Added to control RenderTexture removal
    public class FrameBufferList : List<FrameCache>
    {
        public new void RemoveRange(int Index, int Count)
        {
            List<FrameCache> _RemovalList = GetRange(Index, Count);
            _RemovalList.ForEach (delegate (FrameCache frame)
            {
                frame.Frame.Release();
            }) ;
            base.RemoveRange(Index, Count);
        }
    }

    public delegate void SwitchToPastPayload(float fPastDelta);
    public delegate void SwitchToPresentPayload();

    public event SwitchToPastPayload OnSwitchToPast;
    public event SwitchToPresentPayload OnSwitchToPresent;

    [Header("The Geometry(s) that the stream is rendered to")]
    public GameObject[] ScreenObjects;

    [Header("Add cues here to automate the switch from present to past. This will disable manual Switch On Interval and Switch On Key")]
    public List<TimeCue> _TimeCues;
    private float fNextSwitchTime;
    private bool bInPast = false;
    private int iCurrentCueIndex = 0;

    [Header("If on Cut the feed after a number of seconds")]
    public float fEndOfExperience = 0.0f;
    public Texture _EndOfExperienceCard;
    private bool bExperienceOnTimer = false;

    //TODO if cues are active disable these options
    [Header("Would you like the active geometry to switch between the past and present based on time?")]
    public bool SwitchOnInterval;
    [Header("Would you like the active geometry to switch between the past and present when a key is pressed?")]
    public bool SwitchOnKey;
    [Header("The key to press if key switching is enabled")]
    public string SwitchingKey;
    private bool bKeyDown = false;
    
    [Header("The number of seconds between the transition to the past")]
    public float SecondsToSwitchPast;
    [Header("The number of seconds between the transition to the present")]
    public float SecondsToSwitchPresent;

    private bool bInPresent = true;
    private float fCountdown = 0;

    [Header("Render Texture targets")]
    public RenderTexture	CurrentFrame;
	public RenderTexture	PastFrame;
	public RenderTexture	LiveFrame;

	[Header("Video input. If PopMovieObject is null, a webcam texture is created with the name below")]
#if ENABLE_POPMOVIE
	public PopMovieSimple	PopMovieObject;
#endif
	public string			WebcamName;
	public WebCamTexture	Webcam;
	public int				WebcamWidth = 3840;
	public int				WebcamHeight = 2160;

	[Header("Set this texture and we'll use it instead of a webcam (for when you have no webcam)")]
	public Texture			DummyWebcam;

	[Header("Delay before enabling live feed")]
	[Range(1,10)]
	public float			DelayBeforeShowLiveFeed = 1;

	[Header("Current Frame contains live feed when this is checked")]
	public bool				ShowLiveFeed = false;

	[Header("DELAY - Delay from live feed in seconds")]
	[Range(0.1f,50.0f)]
	public float			DelaySeconds = 0.1f;

	[Range(1,20000)]
	public int				MaxBufferSize;

	public FrameBufferList	FrameBuffer;

	private uint			LastFrameTime = 0;
	private float			WebcamFirstFrameTime = 0;
    
	public bool				StrobeEnabled = true;

	[Header("INTERVAL - How long does the black last (seconds)")]
	[Range(0.01f,10.0f)]
	public float			_StrobeDuration = 0.1f;
	float					StrobeDuration			{	get { return StrobeEnabled ? _StrobeDuration : 0; } }
	[Header("IMAGE - How long does the video last (seconds)")]
	[Range(0.01f,10.0f)]
	public float			NonStrobeDuration = 0.1f;
	private float			StrobeTimer = 0;


	[Header("Optionally set a texture to show when strobing (Colour used if no texture)")]
	public Texture 			StrobeTexture = null;
	public Color			StrobeColour = new Color (0, 0, 0, 1);

	[Header("Clear targets at start to this colour")]
	public bool				InitialClear = true;
	public Color			InitialClearColour = new Color (0, 0, 0, 1);

	void Start()
	{
        if( _TimeCues.Count > 0)
        {
            SwitchOnInterval = false;
            SwitchOnKey = false;
            DelaySeconds = _TimeCues[iCurrentCueIndex].GetDistanceFromLive();
            fNextSwitchTime = _TimeCues[iCurrentCueIndex].GetActivationTime();
        }

        if(fEndOfExperience > 0)
        {
            //Set a bool flag to stop the sim
            bExperienceOnTimer = true;
        }

		//	make a plain black texture. We would use Texture2D.blacktexture, but it has a 0 alpha
		if ( StrobeTexture == null )
		{
			var StrobeTexture2D = new Texture2D(1,1,TextureFormat.RGB24,true);
			StrobeTexture = StrobeTexture2D;
			StrobeTexture2D.SetPixel (0, 0, StrobeColour);
			StrobeTexture2D.Apply ();
		}

		//	clear targets at startup
		if (InitialClear) 
		{
			var ClearTexture = new Texture2D (1, 1, TextureFormat.RGB24, true);
			ClearTexture.SetPixel (0, 0, InitialClearColour);
			ClearTexture.Apply ();
			Graphics.Blit (ClearTexture, CurrentFrame);
			Graphics.Blit (ClearTexture, PastFrame);
		}

#if ENABLE_POPMOVIE
		if (PopMovieObject == null)
		{
			PopMovieObject = GetComponent<PopMovieSimple> ();
		}
#endif
        fCountdown = SecondsToSwitchPast;
	}

	void OnNewFrame(Texture Frame,uint FrameTime)
	{
		//Debug.Log("New frame: " + FrameTime);

		if (FrameBuffer == null)
			FrameBuffer = new FrameBufferList();

		if ( FrameBuffer.Count >= MaxBufferSize )
		{
			//	delete old, or overwrite newest... a more even distribution is a complicated thing!
            //TODO release all frames before removing them from the list
			FrameBuffer.RemoveRange( MaxBufferSize-1, (FrameBuffer.Count - MaxBufferSize)+1 );
		}

		WebcamWidth = Frame.width;
		WebcamHeight = Frame.height;

		var Cache = new FrameCache ();
		var NewFrame = new RenderTexture (Frame.width, Frame.height, 0, PastFrame.format );
		NewFrame.name = "" + FrameTime;
		Graphics.Blit (Frame, NewFrame);
		if ( LiveFrame != null )
			Graphics.Blit( Frame, LiveFrame );
		Cache.Frame = NewFrame;
		Cache.Time = FrameTime;
		FrameBuffer.Add (Cache);

		LastFrameTime = FrameTime;

        
	}

#if ENABLE_POPMOVIE
	void PushBuffer_PopMovie()
	{
		var PopMovie = GetMovie();
		if (PopMovie == null) {
			Debug.Log ("Waiting for movie");
			return;
		}

		uint LastCopyTime = PopMovie.GetLastFrameCopiedMs ();
		if ( LastCopyTime > LastFrameTime )
		{
			OnNewFrame (PopMovieObject.TargetTexture,LastCopyTime);
		}
	}
#endif

	uint GetWebcamTime()
	{
		if ( WebcamFirstFrameTime == 0 )
			WebcamFirstFrameTime = Time.time;

		var TimeMs = (uint)((Time.time - WebcamFirstFrameTime) * 1000.0f);
		return TimeMs;
	}


	void PushBuffer_Webcam()
	{
		if (Webcam == null && DummyWebcam == null)
		{
			try
			{
				if ( WebcamWidth>0 && WebcamHeight>0 )
					Webcam = new WebCamTexture( WebcamName, WebcamWidth, WebcamHeight );
				else
					Webcam = new WebCamTexture( WebcamName );
				Webcam.Play();
				if ( !Webcam.isPlaying )
					throw new System.Exception("No webcam");
			}
			catch {
				Webcam = null;
				DummyWebcam = Texture2D.whiteTexture;
			}
		}

		if ( DummyWebcam != null || (Webcam!=null && Webcam.didUpdateThisFrame) )
		{
			uint LastCopyTime = GetWebcamTime();
			if ( LastCopyTime > LastFrameTime )
			{
				OnNewFrame ( DummyWebcam ? DummyWebcam : Webcam,LastCopyTime);
			}
		}

	}

#if ENABLE_POPMOVIE
	PopMovie GetMovie()
	{
		if (!PopMovieObject)
			return null;
		var Movie = PopMovieObject.Movie;
		return Movie;
	}
#endif

	void PopBuffer()
	{
		//	copy live frame
		if (ShowLiveFeed)
		{
#if ENABLE_POPMOVIE
			if (PopMovieObject) 
			{
				Graphics.Blit (PopMovieObject.TargetTexture, CurrentFrame);
			}
			else
#endif 
			if (DummyWebcam) 
			{
				Graphics.Blit(DummyWebcam, CurrentFrame);
			}
			else if (Webcam)
			{
				Graphics.Blit(Webcam, CurrentFrame);
			}

		}

		//uint LastCopyTime = PopMovie.GetLastFrameCopiedMs ();
		uint LastCopyTime = LastFrameTime;
		uint DelayMs = (uint)(DelaySeconds * 1000);

		//	not done an initial delay yet
		if (DelayMs > LastCopyTime)
			return;


		StrobeTimer += Time.unscaledDeltaTime;
		float TotalDuration = StrobeDuration + NonStrobeDuration;
		StrobeTimer = StrobeTimer % TotalDuration;

		if (StrobeTimer < StrobeDuration) {
			//	strobing
			Graphics.Blit (StrobeTexture, CurrentFrame);
			Graphics.Blit (StrobeTexture, PastFrame);
			return;
		}

		//	else we're showing video


		var DelayedTime = LastCopyTime - DelayMs;

		//	find last frame before delayed time
		int PopIndex = -1;
		for (int i = 0;	i < FrameBuffer.Count;	i++) {
			if (FrameBuffer [i].Time <= DelayedTime)
				PopIndex = i;
			else
				break;
		}
		if (PopIndex == -1)
			return;

        //TODO Here's where we might need to alter the method of blit to the render target 
        //we just need to start at a different index...
        //To replace this cleanly we want to add a delta to PopIndex here
		Graphics.Blit (FrameBuffer [PopIndex].Frame, PastFrame);
		if( !ShowLiveFeed )
			Graphics.Blit (FrameBuffer [PopIndex].Frame, CurrentFrame);
		
		FrameBuffer.RemoveRange (0, PopIndex + 1);
	}

	void DoLiveSwitch()
	{
		if (ShowLiveFeed)
			return;

#if ENABLE_POPMOVIE
		var PopMovie = GetMovie();
		if (PopMovie == null)
			return;

		float LastCopyTime = PopMovie.GetLastFrameCopied ();
		if (LastCopyTime > DelayBeforeShowLiveFeed)
			ShowLiveFeed = true;
#endif
	}

	void Update () 
	{
		//	get new frames
#if ENABLE_POPMOVIE
		if ( PopMovieObject != null )
		{
			PushBuffer_PopMovie();
		}
		else
#endif
		{
			PushBuffer_Webcam();
		}
		
        if(SwitchOnKey && Input.GetAxis(SwitchingKey) > 0.8f && !bKeyDown)
        {
            if(bInPresent)
            {
                SwitchToPast();
                
            }
            else
            {
                SwitchToPresent();

            }
            bKeyDown = true;
        }

        if (Input.GetAxis(SwitchingKey) < 0.1f && bKeyDown)
            bKeyDown = false;

        //Countdown on interval
        if(SwitchOnInterval)
        {
            
                fCountdown -= Time.deltaTime;
                if (fCountdown < 0)
                {
                    if (bInPresent)
                    {
                        SwitchToPast();

                    }
                    else
                    {
                        SwitchToPresent();

                    }
                }
            
        }
        //TODO more clearly define this state
        if (_TimeCues.Count > 0)
        {
            if ((GetWebcamTime() / 1000.0f) >= fNextSwitchTime && iCurrentCueIndex != -1)
            {
                Debug.Log(GetWebcamTime());
                if (bInPast)
                {
                    SwitchToPresent();
                    AdvanceCue();
                    bInPast = false;
                }
                else
                {
                    SwitchToPast();
                    fNextSwitchTime = _TimeCues[iCurrentCueIndex].GetDuration() + _TimeCues[iCurrentCueIndex].GetActivationTime();
                    bInPast = true;
                }
            }
        }

		DoLiveSwitch ();

		//	push out old frames
		PopBuffer();

        fEndOfExperience -= Time.deltaTime;
        if(fEndOfExperience <= 0 && bExperienceOnTimer)
        {
            //TODO
            //Application.Quit();
            Graphics.Blit(_EndOfExperienceCard, CurrentFrame);
            Destroy(this);
        }
	}

    public void TriggerSwitch()
    {
        if (bInPresent)
        {
            SwitchToPast();
            bInPast = false;
        }
        else
        {
            SwitchToPresent();
            bInPast = true;
        }
    }

    void SwitchToPast()
    {
        Debug.Log("Switched to the past");
        Renderer[] myRenderer = new Renderer[ScreenObjects.Length];
        for(int i = 0; i < ScreenObjects.Length; i++)
        {
            myRenderer[i] = ScreenObjects[i].GetComponent<Renderer>();
        }
        foreach (Renderer _R in myRenderer)
        {
            if (_R != null)
            {
                _R.material.SetTexture("_MainTex", PastFrame);
                fCountdown = SecondsToSwitchPresent;
                bInPresent = false;
            }
            else
            {
                Debug.LogError("There is no Renderer on the screen!");
            }
        }
        OnSwitchToPast.Invoke(DelaySeconds);
    }

    void SwitchToPresent()
    {
        Debug.Log("Switched to present");
        Renderer[] myRenderer = new Renderer[ScreenObjects.Length];
        for (int i = 0; i < ScreenObjects.Length; i++)
        {
            myRenderer[i] = ScreenObjects[i].GetComponent<Renderer>();
        }
        foreach (Renderer _R in myRenderer)
        {
            if (_R != null)
            {
                _R.material.SetTexture("_MainTex", CurrentFrame);
                fCountdown = SecondsToSwitchPast;
                bInPresent = true;
            }
            else
            {
                Debug.LogError("There is no Renderer on the screen!");
            }
        }
        OnSwitchToPresent.Invoke();
    }

    void AdvanceCue()
    {
        //Setup the next transition
        if(_TimeCues.Count > (iCurrentCueIndex + 1))
        {
            iCurrentCueIndex++;
            Debug.Log(iCurrentCueIndex);
            TimeCue _NextCue = _TimeCues[iCurrentCueIndex];
            fNextSwitchTime = _NextCue.GetActivationTime();
            DelaySeconds = _NextCue.GetDistanceFromLive();
        }
        else
        {
            Debug.Log("Que #: " + _TimeCues.Count + " Current Que: " + iCurrentCueIndex);
            Debug.Log("End of automated switch list");
            iCurrentCueIndex = -1;
            //Might need a better way to do this
            //Application.Quit();
        }
    }
}
