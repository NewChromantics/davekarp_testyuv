﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerInteraction : MonoBehaviour {


    public FrameBuffering _FBuffer;

    private SteamVR_TrackedObject trackedObj;
    private bool bDown = false;


    private void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    private void Update()
    {
        var device = SteamVR_Controller.Input((int)trackedObj.index);
        
        if(!bDown && device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger)){
            Controller_TriggerClicked();
            bDown = true;
        }
        else if (bDown && device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
        {
            bDown = false;
        }
        
    }

    private void Controller_TriggerClicked()
    {
        Debug.Log("Trigger clicked");
        _FBuffer.TriggerSwitch();
    }
}
