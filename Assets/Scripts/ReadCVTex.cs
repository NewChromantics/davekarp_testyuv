﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ReadCVTex : MonoBehaviour {
    [DllImport("OpenCVCapture",CallingConvention = CallingConvention.Cdecl)]
    public static extern byte[] CaptureFrame();
    [DllImport("OpenCVCapture", CallingConvention = CallingConvention.Cdecl)]
    public static extern void Initialize(int iDevice);
    [DllImport("OpenCVCapture", CallingConvention = CallingConvention.Cdecl)]
    public static extern void Deinitialize();
    [DllImport("OpenCVCapture", CallingConvention = CallingConvention.Cdecl)]
    public static extern double GetFrameHeight();
    [DllImport("OpenCVCapture", CallingConvention = CallingConvention.Cdecl)]
    public static extern double GetFrameWidth();
    [DllImport("OpenCVCapture", CallingConvention = CallingConvention.Cdecl)]
    public static extern bool SetFrameHeight(double y);
    [DllImport("OpenCVCapture", CallingConvention = CallingConvention.Cdecl)]
    public static extern bool SetFrameWidth(double x);

    public RenderTexture _target;

    private Texture2D _tex;

	// Use this for initialization
	void Start () {
        Initialize(0);
        Debug.Log(GetFrameHeight() + " height");
        Debug.Log(GetFrameWidth() + " width");
        _tex = new Texture2D((int)GetFrameWidth(), (int)GetFrameHeight(), TextureFormat.RGBA32, false);
        if(_target.height != _tex.height)
        {
            _target.height = _tex.height;
        }
        if (_target.width != _tex.width)
        {
            _target.width = _tex.width;
        }
    }
	
	void Update() {
        
        byte[] imagData = CaptureFrame();
        _tex.LoadRawTextureData(imagData);
        _tex.Apply();
        Graphics.Blit(_tex, _target);

        if (Input.GetKeyDown(KeyCode.A))
        {
            Deinitialize();
            Destroy(this);
        }
	}
}
