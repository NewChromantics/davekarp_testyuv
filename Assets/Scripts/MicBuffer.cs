﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicBuffer : MonoBehaviour
{
    public FrameBuffering _FBRef;
    public AudioSource _sourceRef;
    [Header("If empty, will use first available mic")]
    public string sMicRef;
    [Header("Is true, will make the realtime audio playback")]
    public bool bPresentPlayback;
    public float fPresentLag;

    private void Start()
    {
        //if we don't have the right mic off the bat we can use the debugger to find it
        if (sMicRef == string.Empty && Microphone.devices[0] != string.Empty)
        {
            sMicRef = Microphone.devices[0];
            Debug.Log("Device " + Microphone.devices[0] + " used");
        }
        else
        {
            foreach (string s in Microphone.devices)
            {
                Debug.Log("Mic device: " + s + " found\n");
            }
        }

        _FBRef.OnSwitchToPast += _FBRef_OnSwitchToPast;
        _FBRef.OnSwitchToPresent += _FBRef_OnSwitchToPresent;
        StartRecording();
    }

    private void OnDestroy()
    {
        _sourceRef.Stop();
    }

    private void _FBRef_OnSwitchToPresent()
    {
        //Debug.Log("Moving Mic to Present");
        MoveToPresent();
    }

    private void _FBRef_OnSwitchToPast(float fPastDelta)
    {
        //Debug.Log("Switch to past event called for mic");
        MovePosition(fPastDelta);
    }

    public void StartRecording()
    {
        _sourceRef.clip = Microphone.Start(sMicRef, true, (int)(_FBRef.fEndOfExperience + 1), 44100);
        while (!(Microphone.GetPosition(null) > 0)) { }
        //Debug.Log("start playing... position is " + Microphone.GetPosition(null));
        if (bPresentPlayback)
        {
            _sourceRef.time = _sourceRef.time - fPresentLag;
            _sourceRef.Play();
        }
    }

    public void MovePosition(float fSeconds)
    {
        if (_sourceRef.time - fSeconds > 0)
        {
            _sourceRef.time = _sourceRef.time - fSeconds;
        }
        else
        {
            _sourceRef.time = 0;
        }
        _sourceRef.Play();
    }

    public void MoveToPresent()
    {
        _sourceRef.Stop();
        StartRecording();
    }
}
